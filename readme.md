You'll need:<br>
sudo pip3 install plexapi<br>
sudo easy_install tvdb_api<br>
sudo apt-get install youtube-dl<br>
sudo chmod a+rx /usr/local/bin/youtube-dl<br>
sudo apt-get install vlc<br>


Changes you'll need to make:<br>
line 483 server ip for sickbeard<br>
line 484 sickbeard api key<br>
line 603 server ip for plex<br>
line 604 plex api key<br>
line 607 plex client name<br>


Voice commands I've coded:<br>
sickbeard recently added - lists the last 5 shows downloaded<br>
Sickbeard today - lists all tv airing today<br>
sickbeard upcoming - lists the next 5 shows airing after today<br>
sickbeard download showname - This will search thetvdb.com and download the match, you can cancel this by pressing the button within 10 seconds of it being added.

stop - plex control<br>
pause - plex control <br>
resume - plex control<br>
play 'tv/movie name' - plays the next unwatched tv show (or resumes if mid way through) or plays a movie.<br>
plex 'tv/movie name' - same<br>
watch 'tv/movie name' - same<br>

The youtube class was written by Mike Redrobe, The podcast and radio classes were written by Winkle ink.<br>
However I've made a few alterations to these, for the podcast and youtube classes i've set the button to pause/resume on single press and stop on hold down.